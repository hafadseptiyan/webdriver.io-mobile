import LoginPage from '../page/login.page.js';

describe('Feature Sign up user', () => {
    beforeEach(async function () {
        await driver.launchApp()
    })
    afterEach(async function () {
        await driver.closeApp()
    })


    it('case: invalid email format', async () => {
        const email    = "email.test@"
        const password = "password"
        const message  = "Please enter a valid email address";

        await LoginPage.signUp(email, password)
        await expect($('//*[contains(@text, "' + message + '")]')).toBeExisting()
    })

    it('case: password less than 8 char', async () => {
        const email    = "test.mail1@mailinator.com"
        const password = ""
        const message  = "Please enter at least 8 characters";

        await LoginPage.signUp(email, password)
        await expect($('//*[contains(@text, "' + message + '")]')).toBeExisting()
    })

    it('case: confirm password is not same', async () => {
        const email    = "test.mail1@mailinator.com"
        const password = "password"
        const message   = "Please enter the same password";

        await LoginPage.signUp(email, password, "not_same")
        await expect($('//*[contains(@text, "' + message + '")]')).toBeExisting()
    })

    it('case: valid registration data', async () => {
        const email    = "test.mail1@yopmail.com"
        const password = "akuntes123"
        const message  = 'You successfully signed up!'

        await LoginPage.signUp(email, password)

        await expect(LoginPage.popupSuccessMessage).toBeExisting()
        await expect(LoginPage.popupSuccessMessage).toHaveTextContaining(message)
        await LoginPage.okPopUp.click()
        await LoginPage.login(email, password)
        await expect(LoginPage.popupSuccessMessage).toBeExisting()
        await expect(LoginPage.popupSuccessMessage).toHaveTextContaining("You are logged in!")
    })

})

